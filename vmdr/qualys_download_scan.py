import http.client
import mimetypes
from codecs import encode

import os
import csv

def download(scan_ref, title):

    conn = http.client.HTTPSConnection("qualysapi.qg2.apps.qualys.eu")
    dataList = []
    boundary = 'wL36Yn8afVp8Ag7AmP8qZ0SA4n1v9T'
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=action;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode("fetch"))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=scan_ref;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    #dataList.append(encode("scan/1566116534.74007"))
    #select scan based on scan_ref
    dataList.append(encode(scan_ref))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=mode;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode("extended"))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=output_format;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode("csv_extended"))
    dataList.append(encode('--'+boundary+'--'))
    dataList.append(encode(''))
    body = b'\r\n'.join(dataList)
    payload = body
    headers = {
    'X-Requested-With': 'QualysPostman',
    'Authorization' :'Basic <>'    
    'Content-type': 'multipart/form-data; boundary={}'.format(boundary)
    }
    conn.request("POST", "/api/2.0/fo/scan/", payload, headers)
    res = conn.getresponse()
    data = res.read()



    #create a folder and put the file in it
    if (os.path.isdir('scan_download')):
        pass
    else:
        os.mkdir('scan_download')

    title = title.replace("/","-")
    #write a file based on how script scanbased.py glob all files
    file = open("./scan_download/" + "Scan_Results_" + title + ".csv","w")
    #write file csv
    line = data.decode("utf-8").split("\n")
    for row in line:
        file.write(row)

    file.close()
    print("downloaded: " + title)
