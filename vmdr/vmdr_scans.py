import http.client
import mimetypes
from codecs import encode


import subprocess
#program to analyze the csv
#import scanbased

#regexp
import re
#library for xml
import xml.etree.ElementTree as ET

#download scan
import qualys_download_scan as qualysapi

def listscan():
    conn = http.client.HTTPSConnection("qualysguard.qg2.apps.qualys.eu")
    dataList = []
    boundary = 'wL36Yn8afVp8Ag7AmP8qZ0SA4n1v9T'
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=action;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode("list"))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=show_ags;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode("1"))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=state;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    #select status:  Running, Finished,Queued,Paused
    dataList.append(encode("Finished"))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=show_op;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode("1"))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=show_status;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode("1"))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=launched_after_datetime;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    #add data from take scans
    #change every time scan a session
    #session from 30 august
    dataList.append(encode("2022-08-30"))
    dataList.append(encode('--'+boundary+'--'))
    dataList.append(encode(''))
    body = b'\r\n'.join(dataList)

    payload = body

    headers = {
    'X-Requested-With': 'QualysPostman',
    'Authorization' :'Basic <>'
    'Content-type': 'multipart/form-data; boundary={}'.format(boundary)
    }
    conn.request("POST", "/api/2.0/fo/scan/", payload, headers)
    res = conn.getresponse()
    data = res.read()
    #print(data.decode("utf-8"))
    #parsing  xml
    parsixml = data.decode("utf-8")
    tree = ET.fromstring(parsixml)

    return tree

#input title of scan then take scan_ref and then download
def main():

    tree = listscan()

    for response in tree:
        for scanlist in response:
            for scan in scanlist:
                #scan[0] is scan_ref
                #every scan[i] is a possible
                #in scan[2] is the title of scan

                if(re.findall("wyswyg", scan[2].text)):
                    print("ref -> " + scan[0].text, "title -> " + scan[2].text)

                    qualysapi.download(scan[0].text,scan[2].text)

if __name__ == '__main__':
    main()
    print("--- launch scanbased analyze---")

    #have scanbased.py in same directory
    subprocess.call("scanbased.py",shell=True)
