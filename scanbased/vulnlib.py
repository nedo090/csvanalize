

#return a text
def textoqid(qid):

    text = ''

    if(qid == "38863"):
        text = "Accettare solo " + \
                "crittografia forte da ssl/tls, cambiare la configurazione del server " + \
                "per permettere solo uno scambio di chiavi con almeno 112 bit di sicurezza, almeno 2048 per diffie-hellman e RSA."

    elif(qid == "86882"):
        text = "Si consiglia  per proteggere la console jmx seguire questa guida https://developer.jboss.org/docs/DOC-12190"

    elif(qid == "11930"):
        text = "Per la CVE-2017-12149 java deserialization aggiornare JBoss o possibile workaround rif. https://bugzilla.redhat.com/show_bug.cgi?id=1486220."

    elif(qid == "11925"):
        text = "Per riferimento a red hat Jboss application server rce consultare https://access.redhat.com/security/cve/CVE-2017-7504"

    elif(qid == "12482"):
        text = "Restringere l’accesso agli utenti non autenticati alla web console. "

    elif(qid == "105581" or qid  == "105930"):
        text = "Se possibile aggiornare la versione di oracle database," + \
                " dato che non viene più supportato dalla casa madre, se non possibile cercare alternative o dismettere la macchina."

    elif(qid == "90783"):
        text = "Si rileva una rce su rdp, non aprire porte non necessarie e applicare le patch necessarie, rif: " + \
                "http://technet.microsoft.com/en-us/security/bulletin/MS12-020"

    elif(qid == "19672"):
        text = "Si segnala che oracle ha rilasciato una patch per “Oracle Database TNS Listener Poison Attack Vulnerability” " + \
                "rif: https://www.oracle.com/technetwork/topics/security/alert-cve-2012-1675-1608180.html"

    elif(qid == "70003"):
        text = "Non permettere l’accesso con passwd nulla sulla sessione NetBIOS."

    elif(qid == "105517"):
        text = "Non mantenere software obsoleti come “JBoss Enterprise Application Platform” " + \
                "aggiornare la versione o dismettere la macchina, esposta ad attacchi e  movimenti laterali."

    elif(qid == "91541"):
        text = "si segnala BlueKeep rce, rif: https://blogs.technet.microsoft.com/msrc/2019/05/14/prevent-a-worm-by-updating-remote-desktop-services-cve-2019-0708/"

    elif(qid == "27210"):
        text = "Restringere l’accesso ad FTP solo ad utenti con autenticazione valida."

    elif(qid == "105859"):
        text = "Si consiglia di aggiornare o dismettere macchine "+ \
                "con sistemi operativi obsoleti non più supportati, se forniscono un servizio migrare " +\
                "il servizio o temporaneamente utilizzare la macchina controllando ogni accesso ad esso."

    elif(qid == "38304"):
        text = "Non supportare protocolli obsoleti di ssh. Disabilitare il supporto alla versione ssh1."

    else:
        text = ""


    return text
