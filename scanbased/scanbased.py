import csv
import glob
import re
from operator import itemgetter
import xlsxwriter
import codecs
import docx
import os
import writescan
import vulnlib

from docx.shared import Pt
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH

files = glob.glob('./scan_download/Scan_Results_*.csv',recursive=True)


category_0day = []
category_os = []
category_software = []
category_remote = []
category_critical = []
category_EOL = []
category_sev4 = []
category_sev5 = []
category_unsync=[]
complessivo=[]
row=[]
source=''
defaultcolor=''
intestazioni=[]

iphits = {}
qidhits = {}


vulnSeverity=['4','5']

def appendto(vuln_type, list):
    list.append([
        vuln_type,
        row[0],#ip
        row[5],#qid
        row[6],#title
        row[8],#severity
        row[9],#port
        row[13],#cve id
        row[16],#threat
        row[17],#impact
        row[18]#solution

    ])

#clear category for the next file
def clearcategory():
    complessivo.clear()
    category_0day.clear()
    category_os.clear()
    category_software.clear()
    category_remote.clear()
    category_critical.clear()
    category_EOL.clear()
    category_unsync.clear()
    category_sev4.clear()
    category_sev5.clear()
    iphits.clear()
    qidhits.clear()

def sort_category():
    category_critical.sort(key=lambda x:x[4],reverse=True)
    category_sev4.sort(key=lambda x:x[0],reverse=True)
    category_sev5.sort(key=lambda x:x[0],reverse=True)
    category_remote.sort(key=lambda x:x[4],reverse=True)
    category_EOL.sort(key=lambda x:x[4],reverse=True)
    category_0day.sort(key=lambda x:x[4],reverse=True)
    category_os.sort(key=lambda x:x[4],reverse=True)
    category_software.sort(key=lambda x:x[4],reverse=True)


#def category based on title of vulnerability
def categorization(row):
    #only confirmed and severity 3,4,5
    #stats for most vuln ip e most qid
    global iphits,qidhits
    if(row[7] =='Vuln' and row[8] in vulnSeverity ):
        #if ip in the dict else add in the index
        if(row[0] in iphits):
            iphits[row[0]] += 1
        else:
            iphits[row[0]] = 1
        #if qid in dict else add 1 in the index
        if(row[5] in qidhits):
            qidhits[row[5]] += 1
        else:
            qidhits[row[5]] = 1
        #vuln relative to operating system
        #check if multiple regex is true
        regex_kernel = re.findall("(K|k)ernel", row[6])
        regex_windowsSecurity = re.search("^Microsoft Windows Security Update*",row[6])
        regex_securityUpdateLinux = re.findall("Security Update for linux",row[6])
        regex_EOLOS = re.search("^EOL/Obsolete Operating System*",row[6])
        #vuln relative to zero day
        zeroday = re.findall("Zero Day",row[6])
        #vuln relative to remote code execution
        remotexecod = re.findall("Remote Code Execution",row[6])
        #vuln for end of life software
        EOL = re.search("^EOL*",row[6])
        #category of qualys
        #if(row[31] == 'Debian' or row[31] =='CentOS' or row[31] =='RedHat' or row[31] =='Ubuntu' or row[31] =='Windows' or row[31] == 'OEl' ):
        if (regex_kernel or regex_windowsSecurity or regex_securityUpdateLinux or regex_EOLOS):
            appendto("OS",category_os)
            if (row[8] =='5'):
                appendto("OS",category_sev5)
            else:
                appendto("OS",category_sev4)
        elif(zeroday):
            appendto("zeroday",category_0day)
            if (row[8] =='5'):
                appendto("zeroday",category_sev5)
            else:
                appendto("zeroday",category_sev4)
        elif(remotexecod):
            #critical if have open port for webserver and there is rce
            if(row[9]):
                appendto("remote",category_critical)
                appendto("remote",category_remote)
            else:
                appendto("remote",category_remote)

            if (row[8] =='5'):
                appendto("remote",category_sev5)
            else:
                appendto("remote",category_sev4)
        elif(EOL):
            if(row[9] ):
                appendto("EOL",category_critical)
                appendto("EOL",category_EOL)
            else:
                appendto("EOL",category_EOL)
            if (row[8] =='5'):
                appendto("EOL",category_sev5)
            else:
                appendto("EOL",category_sev4)
        else:
            #if there is none of the above must be a software vulnerability
            if(row[9] ):
                appendto("software",category_critical)
                appendto("software",category_software)
            else:
                appendto("software",category_software)
            if (row[8] =='5'):
                appendto("software",category_sev5)
            else:
                appendto("software",category_sev4)

        appendto("full",complessivo)


def readFile(filescsv):
    global source,intestazioni
    reader=''
    source= open(filescsv, "r", encoding="latin-1")
    reader = csv.reader(x.replace('\0', '') for x in source)
    #remove header from qualys
    #if the header is already removed comment this section
    #and take name from other source
    for i in range(7):
      #name of tags rename file with this
      #and take number of active host
      if(i == 6):
          activehost = str(row[1])
          filename = row[8]
      #print(next(reader))
      row = next(reader)
    row = next(reader)

    intestazioni = ["tipo vuln",row[0],row[5],row[6],row[8],row[9],row[13],row[16],row[17],row[18]]
    #orrible code for take name of tag
    #replace / with -
    subnet22 = re.findall("/22",filename)
    subnet24 = re.findall("/24",filename)
    subnet26 = re.findall("/26",filename)
    subnet28 = re.findall("/28",filename)


    if(subnet28):
        filename = filename.replace('/28','')

    if(subnet22):
        filename = filename.replace('/22','')

    if(subnet26):
        filename = filename.replace('/26','')

    if(subnet24):
        filename = filename.replace('/24','')


    return filename,row,reader,activehost

def scrivi(array,worksheet,format=defaultcolor,row=1):

    col=0
    for item in array:
        for cell in item:
            worksheet.write(row, col, cell)
            worksheet.set_row(row,cell_format=format)
            col +=1
        row += 1
        col=0

def find(target):
    i = 0
    for x in complessivo:

        if(str(x[2]) == str(target)):
            return i
        else:
            i+=1
    return 0

def intestazione(row,worksheet):
    col=0
    for x in intestazioni:
        worksheet.write(0, col,x)
        col +=1


#write all vuln find
def writevuln(doc):
    doc.add_paragraph("Si segnala che:")

    if(len(category_critical) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_critical)) + " Vulnerabilità critical", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità critical",style="List Bullet")
    if(len(category_0day) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_0day)) + " Vulnerabilità 0 day", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo 0 day",style="List Bullet")
    if(len(category_os) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_os)) + " Vulnerabilità di tipo OS", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo OS",style="List Bullet")
    if(len(category_remote) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_remote)) + " Vulnerabilità di tipo RCE", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo RCE",style="List Bullet")
    if(len(category_EOL) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_EOL)) + " Vulnerabilità di tipo end of life", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo end of life software",style="List Bullet")
    if(len(category_software) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_software)) + " Vulnerabilità di tipo software", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo software",style="List Bullet")

    doc.add_paragraph("Si segnala inoltre che gli IP maggiormente vulnerabili sono i seguenti:")

    ip = sorted(iphits.items(), key=lambda x: x[1], reverse=True)

    #write ip most vuln, if there is more than 3 write only 2 most vuln else write all
    if(len(ip)< 3):
        for i in ip:
            doc.add_paragraph(str(i[0]),style="List Bullet")
    else:
        for i in range(0,2):
            doc.add_paragraph(str(ip[i][0]),style="List Bullet")

    #using a library of qid for write suggestion of the vulnerability 
    for vuln in complessivo:
        doc.add_paragraph(vulnlib.textoqid(vuln[2]))

#write table and title and constant part on docx
#TODO createa summary and title "intestazione"
def writeon_docx(docname,active):

    #create docx document
    doc = docx.Document()
    #create title
    #docname = docname.replace('.xlsx','')

    title = doc.add_heading('Analisi comparativa ' + docname, level=1 )

    #add write a summary
    run = doc.add_paragraph()
    styles = doc.styles
    charstyle = styles.add_style("CommentsStyle", WD_STYLE_TYPE.CHARACTER)
    font = charstyle.font
    font.name = 'Calibri'
    font.size = Pt(16)

    run.add_run("Sommario",style='CommentsStyle')

    #write summary of VULNERABILITIES
    writescan.summary(doc,len(category_sev5),len(category_sev4),active)

    #add table and write list of severity found
    #if there is no vuln check len of category and write to the docx
    if(len(category_sev5) == 0):
        #write there is no sev 5
        doc.add_paragraph(
                'Non sono presenti vulnerabilità di severità 5')
    else:
        doc.add_paragraph(
                'Di seguito si riportano le vulnerabilità di severità 5')

        table = doc.add_table(rows=1,cols=3 )
        cells = table.rows[0].cells
        cells[0].text = 'IP'
        cells[1].text = 'Qid'
        cells[2].text = 'Titolo'
        for item in category_sev5:
            row_cells = table.add_row().cells
            row_cells[0].text = item[1]
            row_cells[1].text = item[2]
            row_cells[2].text = item[3]

        table.style = 'Colorful List'
    #write a predefined text for some vulnerability found

    if(len(category_sev4) == 0):
        #write there is no sev 4
        doc.add_paragraph(
                'Non sono presenti vulnerabilità di severità 4')
    else:
        doc.add_paragraph(
                'Di seguito si riportano le vulnerabilità di severità 4')

        table = doc.add_table(rows=1,cols=3 )
        cells = table.rows[0].cells
        cells[0].text = 'IP'
        cells[1].text = 'Qid'
        cells[2].text = 'Titolo'
        for item in category_sev4:
            row_cells = table.add_row().cells
            row_cells[0].text = item[1]
            row_cells[1].text = item[2]
            row_cells[2].text = item[3]

        table.style = 'Colorful List'

    doc.add_page_break()

    #if > 4 the file excel is created so write summary for it
    if(len(complessivo) > 4):
        writescan.legenda(doc)

    doc.add_heading('AZIONI CONSIGLIATE', level=1)

    #if there is at least 1 vuln write how much based on category
    if(len(complessivo) > 1):
        writevuln(doc)
    else:
        doc.add_paragraph("Non ci sono azioni da consigliare")

    #write prioritizzazione default
    if(len(complessivo) > 4):
        writescan.prioritizzazione(doc)

    #append extension docx
    doc.add_heading('LINK',level=1)
    doc.add_paragraph("Link scansione <>:")
    doc.add_paragraph("Link scansione <>:")

    docname = "analisi " + docname + ".docx"

    print(docname)

    doc.save('./report_scan/' + docname)

#write stats about most vulnerable ip and most vuln find
def stats(workbook):
    #ip più colpiti e qid più frequenti
    global iphits,qidhits
    stats = workbook.add_worksheet('stats')
    ip = sorted(iphits.items(), key=lambda x: x[1], reverse=True)
    qid = sorted(qidhits.items(), key=lambda x: x[1], reverse=True)
    stats.write(0, 0, "TOP IP")
    stats.write(0, 1, "N. volte trovato")
    stats.write(0, 3, "TOP Vuln")
    stats.write(0, 4, "N. volte trovato")
    stats.write(0, 5, "TITLE")
    stats.write(0, 6, "SEVERITY")
    stats.write(0, 7, "CVE")
    stats.write(0, 8, "THREAT")
    stats.write(0, 9, "IMPACT")
    stats.write(0, 10,"SOLUTION")

    linea =1

    #top 3 vuln ip
    if(len(ip) < 3):
        for i in ip :
            stats.write(linea, 0, i[0])
            stats.write(linea, 1, i[1])
            linea +=1
        linea =1
    else:
        for i in range(0,3) :
            stats.write(linea, 0, ip[i][0])
            stats.write(linea, 1, ip[i][1])
            linea +=1
        linea =1

    if(len(qid) < 3):
        for i in qid:
            stats.write(linea, 3, i[0])
            stats.write(linea, 4, i[1])
            index = find(i[0])
            stats.write(linea, 5, complessivo[index][3])
            stats.write(linea, 6, complessivo[index][4])
            stats.write(linea, 7, complessivo[index][6])
            stats.write(linea, 8, complessivo[index][7])
            stats.write(linea, 9, complessivo[index][8])
            stats.write(linea, 10, complessivo[index][9])

            linea+=1
    else:
        for i in range(0,3):
            stats.write(linea, 3, qid[i][0])
            stats.write(linea, 4, qid[i][1])
            index = find(qid[i][0])
            stats.write(linea, 5, complessivo[index][3])
            stats.write(linea, 6, complessivo[index][4])
            stats.write(linea, 7, complessivo[index][6])
            stats.write(linea, 8, complessivo[index][7])
            stats.write(linea, 9, complessivo[index][8])
            stats.write(linea, 10, complessivo[index][9])

            linea+=1


def writeon_xlsx(filename):

    filename = filename + ".xlsx"

    workbook = xlsxwriter.Workbook('./report_scan/'+ filename)
    defaultcolor=workbook.add_format({'bg_color': '#FFFFFF'})

    #create file with name of tag
    data_format_oro = workbook.add_format({'bg_color': '#c29436'})
    data_format_remote = workbook.add_format({'bg_color': '#e50000'})
    data_format_0day = workbook.add_format({'bg_color': '#0096FF'})
    data_format_EOL = workbook.add_format({'bg_color': '#c46404'})

    worksheet_completo = workbook.add_worksheet('completo')
    intestazione(row,worksheet_completo)
    worksheet_critical = workbook.add_worksheet('CRITICAL')
    intestazione(row,worksheet_critical)
    worksheet_sev5 = workbook.add_worksheet('SEV 5')
    intestazione(row,worksheet_sev5)
    worksheet_sev4 = workbook.add_worksheet('SEV 4')
    intestazione(row,worksheet_sev4)
    worksheet_0day = workbook.add_worksheet('0Day')
    intestazione(row,worksheet_0day)
    worksheet_os = workbook.add_worksheet('OS')
    intestazione(row,worksheet_os)
    worksheet_rce = workbook.add_worksheet('RCE')
    intestazione(row,worksheet_rce)
    worksheet_eols = workbook.add_worksheet('EOL - Software')
    intestazione(row,worksheet_eols)
    worksheet_software = workbook.add_worksheet('Software')
    intestazione(row,worksheet_software)

    i=1

    scrivi(category_os,worksheet_completo,format=data_format_oro,row=i)
    scrivi(category_os,worksheet_os)
    i+=len(category_os)
    scrivi(category_remote,worksheet_completo,format=data_format_remote,row=i)
    scrivi(category_remote,worksheet_rce)
    i+=len(category_remote)
    scrivi(category_0day,worksheet_completo,format=data_format_0day,row=i)
    scrivi(category_0day,worksheet_0day)
    i+=len(category_0day)
    scrivi(category_EOL,worksheet_completo,format=data_format_EOL,row=i)
    scrivi(category_EOL,worksheet_eols)
    i+=len(category_EOL)
    scrivi(category_software,worksheet_completo,row=i)
    scrivi(category_software,worksheet_software)
    i+=len(category_software)
    scrivi(category_critical,worksheet_critical)
    scrivi(category_sev5,worksheet_sev5)
    scrivi(category_sev4,worksheet_sev4)

    #create stats on excel
    stats(workbook)
    #close excel file
    workbook.close()


def main():
    global row,source,defaultcolor
    for filescsv in files:

        filename,row,reader,activehost = readFile(filescsv)

        #create a folder and put the file in it
        if (os.path.isdir('report_scan')):
            pass
        else:
            os.mkdir('report_scan')

        #first line
        #categorization
        try:
          for row in reader:
              categorization(row)
        except IndexError:
          pass
        #close reader csv
        source.close()

        #sort based on severity
        sort_category()

        #create and write a docx
        writeon_docx(filename,activehost)
        #if the len of complessivo is more than 4 create the excel
        if len(complessivo) > 4:
            writeon_xlsx(filename)

        # clear every category for the next file
        clearcategory()

        filename = filename.replace('.xlsx','')
        activehost = ''
        print("done ->" + filename)



if __name__ == '__main__':
    main()
