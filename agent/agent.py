import csv
import glob
import re
from operator import itemgetter
import xlsxwriter
import codecs
import os
import docx
import writeagent
import vulnlib

from docx.shared import Pt
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH

files = glob.glob('Scan_Report_*.csv',recursive=True)
#choose column in base of number
#row[0] == ip
#row[6] == QID
#row[7] == title
#row[8] == vuln status (active,new, re-opened)
#row[9] == Type
#row[10] == severity
#row[11] == port  (check if null string)
#row[19] == cveid (check if null string)
#row[22] == threat
#row[23] == impact
#row[24] == solution
#row[25] == Exploitability (check if null string)
#row[27] == result
#row[31] == category

category_0day = []
category_os = []
category_software = []
category_remote = []
category_critical = []
category_EOL = []
category_sev4 = []
category_sev5 = []
category_unsync=[]
complessivo = []
row = []
source=''
defaultcolor=''
intestazioni=[]
ports=[]
#dict for stats most vuln e most qid
iphits = {}
qidhits = {}


vulnSeverity=['4','5']
def appendto(vuln_type,list):
    list.append([
        vuln_type,
        row[0],
        row[6],
        row[7],
        row[10],
        row[11],
        row[19],
        row[22],
        row[23],
        row[24],
        row[25],
        row[27],
        row[31]
    ])



#clear category for the next file
def clearcategory():

    complessivo.clear()
    category_0day.clear()
    category_os.clear()
    category_software.clear()
    category_remote.clear()
    category_critical.clear()
    category_EOL.clear()
    category_unsync.clear()
    category_sev4.clear()
    category_sev5.clear()
    iphits.clear()
    qidhits.clear()
#

#sort based on severity
def sort_category():
    category_critical.sort(key=lambda x:x[4],reverse=True)
    category_sev4.sort(key=lambda x:x[0],reverse=True)
    category_sev5.sort(key=lambda x:x[0],reverse=True)
    category_remote.sort(key=lambda x:x[4],reverse=True)
    category_EOL.sort(key=lambda x:x[4],reverse=True)
    category_0day.sort(key=lambda x:x[4],reverse=True)
    category_os.sort(key=lambda x:x[4],reverse=True)
    category_software.sort(key=lambda x:x[4],reverse=True)

#append on a list all length of all category with the label
#then sort the list based on number decrease to max number to min then return
def maxcategory():

    list = []

    list.append(["EOL",len(category_EOL)])
    list.append(["OS",len(category_os)])
    list.append(["0day",len(category_0day)])
    list.append(["RCE",len(category_remote)])
    list.append(["critical",len(category_critical)])
    list.append(["software",len(category_software)])


    #sort based on len of category 
    list.sort(key=lambda x:x[1],reverse=True)


    return list

def categorization(row):
    #only confirmed and severity 4,5
    #stats for most vuln ip e most qid
    global iphits,qidhits,complessivo
    if(row[9] =='Vuln' and row[10] in vulnSeverity ):
        #if ip in the dict else add in the index
        if(row[0] in iphits):
            iphits[row[0]] += 1
        else:
            iphits[row[0]] = 1
        #if qid in dict else add 1 in the index
        if(row[6] in qidhits):
            qidhits[row[6]] += 1
        else:
            qidhits[row[6]] = 1
        #vuln relative to operating system
        #check if multiple regex is true
        regex_kernel = re.findall("(K|k)ernel", row[7])
        regex_windowsSecurity = re.search("^Microsoft Windows Security Update*",row[7])
        regex_securityUpdateLinux = re.findall("Security Update for linux",row[7])
        regex_EOLOS = re.search("^EOL/Obsolete Operating System*",row[7])
        #vuln relative to zero day
        zeroday = re.findall("Zero Day",row[7])
        #vuln relative to remote code execution
        remotexecod = re.findall("Remote Code Execution",row[7])
        #vuln for end of life software
        EOL = re.search("^EOL*",row[7])
        #category of qualys
        #if(row[31] == 'Debian' or row[31] =='CentOS' or row[31] =='RedHat' or row[31] =='Ubuntu' or row[31] =='Windows' or row[31] == 'OEl' ):
        if (regex_kernel or regex_windowsSecurity or regex_securityUpdateLinux or regex_EOLOS):
            appendto("OS",category_os)
            if (row[10] =='5'):
                appendto("OS",category_sev5)
            else:
                appendto("OS",category_sev4)
        elif(zeroday):
            appendto("zeroday",category_0day)
            if (row[10] =='5'):
                appendto("zeroday",category_sev5)
            else:
                appendto("zeroday",category_sev4)
        elif(remotexecod):
            #critical if have open port for webserver and there is rce
            if(row[11]):
                appendto("remote",category_critical)
                appendto("remote",category_remote)
            else:
                appendto("remote",category_remote)

            if (row[10] =='5'):
                appendto("remote",category_sev5)
            else:
                appendto("remote",category_sev4)
        elif(EOL):
            if(row[11] ):
                appendto("EOL",category_critical)
                appendto("EOL",category_EOL)
            else:
                appendto("EOL",category_EOL)
            if (row[10] =='5'):
                appendto("EOL",category_sev5)
            else:
                appendto("EOL",category_sev4)
        else:
            #if there is none of the above must be a software vulnerability
            if(row[11] ):
                appendto("software",category_critical)
                appendto("software",category_software)
            else:
                appendto("software",category_software)
            if (row[10] =='5'):
                appendto("software",category_sev5)
            else:
                appendto("software",category_sev4)

        appendto("full",complessivo)


def readFile(filescsv):
    global source,intestazioni
    reader=''
    source= open(filescsv, "r", encoding="latin-1")
    reader = csv.reader(x.replace('\0', '') for x in source)
    #remove header from qualys
    #if the header is already removed comment this section
    #and take name from other source
    for i in range(10):
      #name of tags rename file with this
      if(i == 6):
          name = row[6]
          activehost = row[2]
      if(i == 9):
          avgsecrisk = row[1]

      #print(next(reader))
      row = next(reader)
    row = next(reader)
    intestazioni = ["tipo vuln",row[0],row[6],row[7],row[10],row[11],row[19],row[22],row[23],row[24],row[25],row[27],row[31]]
    #orrible code for take name of tag
    #TODO try catch error if name is not there
    #if no name then assign another name
    #TODO try to
    try:

        filename = name.split(':')[1].split(';')[0]

    except IndexError:
        #if multiple file have no tag got error for file with same name
        filename = "NOtagname"
    filename = filename + ".xlsx"
    return filename,avgsecrisk,activehost, row,reader

#parse file Agents* from 'cloud agent' for the unsync agent
def readSync(tags):
    files = glob.glob('Agents_gpc*.csv',recursive=True)
    fine = False
    for file in files:

        if(fine==True):
            break
        category_unsync.clear()
        source= open(file, "r", encoding="latin-1")
        reader = csv.reader(x.replace('\0', '') for x in source)
        for i in range(6):
            if(fine==True):
                break
            #write first line
            if(i==4):
                category_unsync.append(riga)
            #take first name from tag
            if(i == 5):
                try:
                    val = tags.lower().split('.')[0].strip()
                except Exception as e:
                    val = tags.lower()
                try:
                    #if the name of tags is in cell for check
                    corretto = val in riga[9].lower()
                except Exception as e:
                    corretto = val in riga[10].lower()
                if (corretto):
                    fine = True
                    while(True):
                        try:
                          category_unsync.append(riga)
                          riga=next(reader)
                        except:
                          break
            else:
                riga=next(reader)

        source.close()

def scrivi(array,worksheet,format=defaultcolor,row=1):

    col=0
    for item in array:
        for cell in item:
            worksheet.write(row, col, cell)
            worksheet.set_row(row,cell_format=format)
            col +=1
        row += 1
        col=0

def find(target):
    i = 0
    for x in complessivo:

        if(str(x[2]) == str(target)):
            return i
        else:
            i+=1
    return 0
#write stats about most vulnerable ip and most vuln find
def stats(workbook):
    #ip più colpiti e qid più frequenti
    global iphits,qidhits
    stats = workbook.add_worksheet('stats')
    ip = sorted(iphits.items(), key=lambda x: x[1], reverse=True)
    qid = sorted(qidhits.items(), key=lambda x: x[1], reverse=True)
    stats.write(0, 0, "TOP IP")
    stats.write(0, 1, "N. volte trovato")
    stats.write(0, 3, "TOP Vuln")
    stats.write(0, 4, "N. volte trovato")
    stats.write(0, 5, "TITLE")
    stats.write(0, 6, "SEVERITY")
    stats.write(0, 7, "CVE")
    stats.write(0, 8, "THREAT")
    stats.write(0, 9, "IMPACT")
    stats.write(0, 10,"SOLUTION")
    linea = 1
    #make range if < 10
    for i in range(0,10):
        stats.write(linea, 0, ip[i][0])
        stats.write(linea, 1, ip[i][1])
        linea +=1
    linea =1
    for i in range(0,10):
        stats.write(linea, 3, qid[i][0])
        stats.write(linea, 4, qid[i][1])
        index = find(qid[i][0])
        stats.write(linea, 5 ,complessivo[index][3])
        stats.write(linea, 6 ,complessivo[index][4])
        stats.write(linea, 7,complessivo[index][6])
        stats.write(linea, 8,complessivo[index][7])
        stats.write(linea, 9,complessivo[index][8])
        stats.write(linea, 10,complessivo[index][9])

        linea+=1


def intestazione(row,worksheet):
    col=0
    for x in intestazioni:
        worksheet.write(0, col,x)
        col +=1

def writeon_xlsx(filename):

    workbook = xlsxwriter.Workbook('./report_agent/'+ filename)
    defaultcolor=workbook.add_format({'bg_color': '#FFFFFF'})
    #create file with name of tag
    data_format_oro = workbook.add_format({'bg_color': '#c29436'})
    data_format_remote = workbook.add_format({'bg_color': '#e50000'})
    data_format_0day = workbook.add_format({'bg_color': '#0096FF'})
    data_format_EOL = workbook.add_format({'bg_color': '#c46404'})
    worksheet_completo = workbook.add_worksheet('completo')
    intestazione(row,worksheet_completo)
    worksheet_critical = workbook.add_worksheet('CRITICAL')
    intestazione(row,worksheet_critical)
    worksheet_sev5 = workbook.add_worksheet('SEV 5')
    intestazione(row,worksheet_sev5)
    worksheet_sev4 = workbook.add_worksheet('SEV 4')
    intestazione(row,worksheet_sev4)
    worksheet_0day = workbook.add_worksheet('0Day')
    intestazione(row,worksheet_0day)
    worksheet_os = workbook.add_worksheet('OS')
    intestazione(row,worksheet_os)
    worksheet_rce = workbook.add_worksheet('RCE')
    intestazione(row,worksheet_rce)
    worksheet_eols = workbook.add_worksheet('EOL - Software')
    intestazione(row,worksheet_eols)
    worksheet_software = workbook.add_worksheet('Software')
    intestazione(row,worksheet_software)
    worksheet_unsync = workbook.add_worksheet('Unsync')


    i=1
    # write every category on workshhet completo and on every category
    #
    scrivi(category_os,worksheet_completo,format=data_format_oro,row=i)
    scrivi(category_os,worksheet_os)
    i+=len(category_os)
    scrivi(category_remote,worksheet_completo,format=data_format_remote,row=i)
    scrivi(category_remote,worksheet_rce)
    i+=len(category_remote)
    scrivi(category_0day,worksheet_completo,format=data_format_0day,row=i)
    scrivi(category_0day,worksheet_0day)
    i+=len(category_0day)
    scrivi(category_EOL,worksheet_completo,format=data_format_EOL,row=i)
    scrivi(category_EOL,worksheet_eols)
    i+=len(category_EOL)
    scrivi(category_software,worksheet_completo,row=i)
    scrivi(category_software,worksheet_software)
    i+=len(category_software)
    scrivi(category_critical,worksheet_critical)
    scrivi(category_sev5,worksheet_sev5)
    scrivi(category_sev4,worksheet_sev4)
    scrivi(category_unsync,worksheet_unsync,row=0)

    #stats(workbook)

    workbook.close()

#add everything
def azionicons(doc):

    doc.add_heading('AZIONI CONSIGLIATE', level=2)
    doc.add_paragraph("Si segnala che:")

    if(len(category_critical) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_critical)) + " Vulnerabilità critical", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità critical",style="List Bullet")
    if(len(category_0day) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_0day)) + " Vulnerabilità 0 day", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo 0 day",style="List Bullet")
    if(len(category_os) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_os)) + " Vulnerabilità di tipo OS", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo OS",style="List Bullet")
    if(len(category_remote) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_remote)) + " Vulnerabilità di tipo RCE", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo RCE",style="List Bullet")
    if(len(category_EOL) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_EOL)) + " Vulnerabilità di tipo end of life", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo end of life software",style="List Bullet")
    if(len(category_software) > 0):
        doc.add_paragraph("Vengono rilevate " + str(len(category_software)) + " Vulnerabilità di tipo software", style="List Bullet")
    else:
        doc.add_paragraph("Non vengono rilevate vulnerabilità di tipo software",style="List Bullet")

    doc.add_paragraph("Si segnala inoltre che gli IP maggiormente vulnerabili sono i seguenti:")

    ip = sorted(iphits.items(), key=lambda x: x[1], reverse=True)

    #write ip most vuln, if there is more than 4 write only 3 most vuln else write all
    if(len(ip)< 4):
        for i in ip:
            doc.add_paragraph(str(i[0]),style="List Bullet")
    else:
        for i in range(0,3):
            doc.add_paragraph(str(ip[i][0]),style="List Bullet")

    doc.add_paragraph("In particolare si segnala:")

    #using a library of qid for write suggestion of the vulnerability
    for vuln in complessivo:
        doc.add_paragraph(vulnlib.textoqid(vuln[2]))


def writeon_docx(docname,avgsecrisk,activehost):
    global complessivo, category_sev5, category_sev4
    #create docx document
    doc = docx.Document()
    #create title
    docname = docname.replace('.xlsx','')

    doc.add_heading('Analisi agent ' + docname, level=1 )

    writeagent.readme(doc)

    max = maxcategory()

    writeagent.summary(doc,max, len(category_sev5), len(category_sev4),avgsecrisk,activehost)

    writeagent.legenda(doc)

    azionicons(doc)

    #writeagent.prioritizzazione(doc)

    #append extension docx
    writeagent.link(doc)

    docname =  docname  + " analisi.docx"

    doc.save('./report_agent/' + docname)

    docname.replace('.docx','')

def main():
    global row,source,defaultcolor
    for filescsv in files:

        filename,avgsecrisk,activehost,row,reader = readFile(filescsv)
        try:
            #find the right csv based on named of tag
            readSync(filename)
        except Exception as e:
            raise
        #create a folder with name report in same directory
        if (os.path.isdir('report_agent')):
            pass
        else:
            os.mkdir('report_agent')

        #first line
        try:
          for row in reader:
              categorization(row)
        except IndexError:
          pass
        #close csv
        source.close()

        #sorting category
        sort_category()

        #create a excel file with category
        writeon_xlsx(filename)

        #create a file docx for analyze
        writeon_docx(filename,avgsecrisk,activehost)

        #clear all category for the next file
        clearcategory()


        print("done ->" + filename)



if __name__ == '__main__':
    main()
