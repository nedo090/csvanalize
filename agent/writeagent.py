import docx
from docx.shared import Pt
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH
#try to write everything in calibri
#write azioni consigliate

from docx.oxml.shared import qn
from docx.oxml.xmlchemy import OxmlElement

def readme(doc):

    doc.add_paragraph(
        "In questo file si riportano i risultati della scansione autenticata in cui "
        "offre una prospettiva offensiva di un utente che ha una visione totale sul sistema.")


def summary(doc,max, sev5, sev4, avrisk, activehost):

    doc.add_heading('SUMMARY OF VULNERABILITIES', level=2)

    doc.add_paragraph('Dalla seguente Summary si evince che:')

    doc.add_paragraph("Il Security Risk Average è di " + str(avrisk) + ", calcolato su " + str(activehost) +  " Host attivi",style='List Bullet')
    #add complessivo on and add all severity
    doc.add_paragraph(
    "Vengono rilevate "  + str(sev5) + " di severity 5 e " + str(sev4) + " di severity 4 ",style='List Bullet')
    #calculate category most present
    doc.add_paragraph("Le vulnerabilità sono suddivise in categorie; le categorie più presenti sono " + str(max[0][0]) + " e " + str(max[1][0]) ,style='List Bullet')


def cell_background(cell, fill, color=None, val=None):
    cell_properties = cell._element.tcPr
    try:
        cell_shading = cell_properties.xpath('w:shd')[0]  # in case there's already shading
    except IndexError:
        cell_shading = OxmlElement('w:shd') # add new w:shd element to it
    if fill:
        cell_shading.set(qn('w:fill'), fill)  # set fill property, respecting namespace
    if color:
        pass # TODO
    if val:
        pass # TODO
    cell_properties.append(cell_shading)


#TODO try to add table with color
def legenda(doc):
    doc.add_heading('LEGENDA', level=2)

    doc.add_paragraph("Nella tabella sotto riportata vengono catalogate tutte le vulnerabilità riscontrate:")
    doc.add_paragraph(
    'Il foglio excel e` stato organizzato in fogli singoli nella seguente maniera:')

    doc.add_paragraph('CRITICAL:',style='List Bullet')
    doc.add_paragraph(
    "Sono indicate come CRITICAL tutte le vulnerabilità "
    "in cui è presente una RemoteCodeExecution o EOL/Software con porta aperta "
    "verso l’esterno dando quindi possibilità a degli eventuali attaccanti di "
    "usufruire di queste falle. Si invita ad attenzionare queste vulnerabilità al più presto.")

    doc.add_paragraph('EOL:',style='List Bullet')
    doc.add_paragraph(
    "Queste vulnerabilità sono "
    "legate a dei software non più supportati (che non ricevono più patch di alcun tipo) "
    "che possono portare a bug nel sistema, eventuali privilege escalation e altre vulnerabilità.")

    doc.add_paragraph('RCE:',style='List Bullet')
    doc.add_paragraph(
    "Queste vulnerabilità sono legate alla possibilità per un "
    "attaccante di eseguire del codice arbitrario da fonte remota. Potendo eseguire "
    "del codice queste vulnerabilità risultano particolarmente critiche in quanto "
    "danno la possibilità all’attaccante di ottenere il controllo della macchina "
    "ed ottenere un accesso persistente.")

    doc.add_paragraph('ZeroDay:',style='List Bullet')
    doc.add_paragraph(
    "Si considera una vulnerabilità "
    "ZeroDay quando ancora nessuna patch è presente per correggere questo problema, "
    "è lo stato che intercorre tra la scoperta e la patch della vulnerabilità stessa. "
    "In questo caso si consiglia di sospendere - per quanto possibile - l\'attività "
    "del servizio fino a nuova patch di sicurezza")

    doc.add_paragraph('OS:',style='List Bullet')
    doc.add_paragraph(
    "Vulnerabilità relativa al sistema operativo più difficili "
    "da correggere in quanto potrebbero richiedere un riavvio o, in casi estremi, "
    "la sostituzione del sistema operativo.")

    doc.add_paragraph('Software:',style='List Bullet')
    doc.add_paragraph("Vulnerabilità relative ai software installati sul sistema operativo.")

    doc.add_paragraph('Unsync:',style='List Bullet')
    doc.add_paragraph(
    "Dispositivi che hanno una data di sincronizzazione maggiore di 60 giorni. "
    "Controllare l’eventuale attività di queste macchine e in caso di dismissione comunicarcelo.")


    table = doc.add_table(rows=5,cols=2 )
    cells = table.rows[0].cells
    cells[0].text = 'Colore'
    cells[1].text = 'Significato'

    row_cells = table.rows[1].cells
    row_cells[0].text = "ORO"
    row_cells[1].text = "Vulnerabilità relativa al sistema operativo \t "
    cell_background(row_cells[0] ,"#c29436")

    row_cells = table.rows[2].cells
    row_cells[0].text = "ROSSO"
    row_cells[1].text = "Remote code execution"
    cell_background(row_cells[0] ,"#e50000")

    row_cells = table.rows[3].cells
    row_cells[0].text = "BLU"
    row_cells[1].text = "Zero day"
    cell_background(row_cells[0] ,"#0096FF")

    row_cells = table.rows[4].cells
    row_cells[0].text = "MARRONE"
    row_cells[1].text = "End Of Life (SOFTWARE)"
    cell_background(row_cells[0] ,"#c46404")


def prioritizzazione(doc):
    doc.add_heading('PRIORITIZZAZIONE', level=2)
    doc.add_paragraph(
    "Per facilitare la visione e la lettura di questo documento eseguiamo "
    "una prioritizzazione sul tag relativo alla sottorete.")
    doc.add_paragraph(
    "Nella figura seguente è possibile prendere visione del numero di Vulnerabilità "
    "confermate raggruppate per severity, il numero di asset e il numero totale di vulnerabilità confermate.")
    #
    run = doc.add_paragraph()
    styles = doc.styles
    charstyle = styles.add_style("prio", WD_STYLE_TYPE.CHARACTER)
    font = charstyle.font
    font.name = 'Calibri'
    font.size = Pt(9)
    run.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
    run.add_run('Figura 3 dashboard prioritization\n',style='prio').italic = True

    doc.add_paragraph("Vengono riportate le vulnerabilità di severity 5 più impattanti")
    run.add_run('Figura 4 vulnerabilità severità 5\n',style='prio').italic = True

    doc.add_paragraph("Vengono riportate le vulnerabilità di severity 4 più impattanti")
    run.add_run('Figura 5 vulnerabilità severità 4\n',style='prio').italic = True

def link(doc):
    doc.add_heading('LINK',level=2)
    doc.add_paragraph("Link scansione:")
