
import vulnlibrary
from docx.shared import Inches, Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH

from datetime import date


#insert header and footer image
#the png header and footer must be in the same folder of the script
def headerfooter(doc):

    header = doc.sections[0].header
    footer = doc.sections[0].footer


    paragraph_header = header.paragraphs[0]
    paragraph_footer = footer.paragraphs[0]

    logo_run_header = paragraph_header.add_run()
    logo_run_header.add_picture("header.png", width=Inches(2.5))

    logo_run_footer = paragraph_footer.add_run()
    logo_run_footer.add_picture("footer.png", width=Inches(5.8))

    paragraph_footer.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

def makebold(cells):
    for paragraphs in cells.paragraphs:
        for run in paragraphs.runs:
            run.font.bold = True
def tab(doc):
    table = doc.add_table(rows=4,cols=3 )
    cells = table.rows[0].cells
    cells[0].text = ''
    cells[1].text = 'Nome'
    makebold(cells[1])
    cells[2].text = 'Ruolo'
    makebold(cells[2])

    cells = table.rows[1].cells
    cells[0].text = 'Autore'

    cells[1].text = 'Giuseppe Mattia Spinelli'
    cells[2].text = 'Cybersecurity Analyst'

    cells = table.rows[2].cells
    cells[0].text = 'Autore'
    cells[1].text = 'Luca Montemaggi'
    cells[2].text = 'Cybersecurity Analyst'

    cells = table.rows[3].cells
    cells[0].text = 'Revisione'
    cells[1].text = 'Manuela Sforza'
    cells[2].text = 'CISO GPI Cyberdefence'

    table.style = 'Table Grid'

    doc.add_paragraph(" ")

    tables = doc.add_table(rows=3,cols=4)
    cells = tables.rows[0].cells
    cells[0].text = 'Versione'
    makebold(cells[0])
    cells[1].text = 'Data'
    makebold(cells[1])
    cells[2].text = 'Paragrafo'
    makebold(cells[2])
    cells[3].text = 'Modifiche'
    makebold(cells[3])


    cells = tables.rows[1].cells
    cells[0].text = '1.0'
    cells[1].text = str(date.today())
    cells[2].text = "-"
    cells[3].text = "Prima emissione"

    tables.style = 'Table Grid'



def readme(doc):
    doc.add_heading('Readme', level=1)

    doc.add_paragraph("In questo file vengono elencate tutte le attività di test che sono stato effettuate, in particolare si troverà:")

    run = doc.add_paragraph()
    run.add_run("Scansione di frontend non autenticata:").bold = True
    run.style = 'List Bullet'


    doc.add_paragraph(
    "Viene fatta con una sonda esterna dal software cloud Qualys, è possibile "
    "anche utilizzare una sonda all’interno della stessa sottorete del servizio "
    "esposto, senza esporre il servizio su internet.")

    run = doc.add_paragraph()
    run.add_run("Scansione di frontend autenticata:").bold = True
    run.style = 'List Bullet'

    doc.add_paragraph(
    "Fornendoci le credenziali di autenticazione con api "
    "(postman, o altro) o attraverso username e passwd, viene fatta una scansione "
    "con profilo autenticato, come se un attaccante riuscisse a ottenere delle "
    "credenziali di accesso e entrare nell’applicazione. Generalmente un profilo "
    "autenticato ha maggiori punti vulnerabili.")

    run = doc.add_paragraph()
    run.add_run("Scansione di backend non autenticata:").bold = True
    run.style = 'List Bullet'

    doc.add_paragraph(
    "Si va a scansionare l’IP privato del server in cui è hostato il servizio "
    "con una sonda interna alla sottorete del server stesso.")

    run = doc.add_paragraph()
    run.add_run("Scansione di backend autenticata:").bold = True
    run.style = 'List Bullet'

    doc.add_paragraph(
    "Grazie ad un agent installato all’interno del server questa scansione "
    "offre una prospettiva offensiva di un utente che ha una visione totale sul sistema.")

    doc.add_paragraph("All’interno di ogni scansione è possibile trovare il QID: "
    "il numero ID Qualys univoco assegnato alla vulnerabilità. Viene inserito all’interno "
    "di questa analisi per facilitare la ricerca delle informazioni di ogni singola "
    "vulnerabilità (minaccia, impatto, soluzioni etc..) all’interno del report "
    "completo allegato ad ogni scansione.")


    doc.add_heading('Dati tecnici ambiente',level=1)

    doc.add_heading('Requisiti compliance',level=1)
    doc.add_paragraph('Verificare il contratto')



def noauth(doc,nvuln,secrisk,cvss_min,cvss_max, vuln, response):
    doc.add_heading("Scansione di vulnerabilità non autenticata",level=1)

    doc.add_paragraph(
    "Dalla scansione non autenticata del front-end, vengono rilevati " + nvuln +
    " indicatori di vulnerabilità e il security risk è " + secrisk +
    " (mappato su CVSS Score Base MIN " + cvss_min + " - MAX " + cvss_max + ").")

    run = doc.add_paragraph()
    p = run.add_run("Mappatura sulle OWASP top 10")
    p.bold = True
    p.font.size = Pt(13)

    doc.add_heading("Sintesi delle criticità",level=1)
    doc.add_paragraph("Riportiamo le severità maggiori:")

    for v in vuln:

        run = doc.add_paragraph()
        p = run.add_run(v[0] + " " + v[1])
        run.style = 'List Bullet'
        p.bold = True
        p.font.size = Pt(12)

        #run = doc.add_heading(v[0] + " " + v[1], level=3)

        #run.style = 'List Bullet'

        #write text relative to this qid
        doc.add_paragraph(vulnlibrary.textoqid(v[0], response))

    doc.add_paragraph("Per il report completo si riporta alla visione del documento:")

def auth(doc,nvuln,secrisk,cvss_min,cvss_max, vuln, response):
    doc.add_heading("Scansione di vulnerabilità autenticata",level=1)

    doc.add_paragraph(
    "Dalla scansione autenticata del front-end, vengono rilevati " + nvuln +
    " indicatori di vulnerabilità e il security risk è " + secrisk +
    " (mappato su CVSS Score Base MIN " + cvss_min + " - MAX " + cvss_max + ").")

    run = doc.add_paragraph()
    p = run.add_run("Mappatura sulle OWASP top 10")
    p.bold = True
    p.font.size = Pt(13)


    doc.add_heading("Sintesi delle criticità", level=1)
    doc.add_paragraph("riportiamo le severità maggiori:")

    for v in vuln:

        run = doc.add_paragraph()
        p = run.add_run(v[0] + " " + v[1])
        run.style = 'List Bullet'
        p.bold = True
        p.font.size = Pt(12)

        #doc.add_paragraph(v[0] + " " + v[1],style='List Bullet')

        #write text relative to this qid
        doc.add_paragraph(vulnlibrary.textoqid(v[0], response))

    doc.add_paragraph("Per il report completo si riporta alla visione del documento:")

def backend(doc):

    doc.add_heading("Scansione Backend (NO Auth)",level=1)
    doc.add_paragraph("In questa sezione vengono riportati i risultati della scansione del server Backend noauth")
    doc.add_paragraph("Per il report completo si riporta alla visione del documento:")

    doc.add_heading("Scansione Backend (Auth)",level=1)
    doc.add_paragraph("In questa sezione vengono riportati i risultati della scansione del server Backend auth")
    doc.add_paragraph("Per il report completo si riporta alla visione del documento:")
