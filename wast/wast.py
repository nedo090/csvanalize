import sys
import csv
import glob
import re
from operator import itemgetter
import xlsxwriter
import codecs
import docx
import os

import writewast

from docx.shared import Pt
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH

MAXint = sys.maxsize

files = glob.glob('Scan_Report_*.csv',recursive=True)


vulnSeverity = ['2','3','4','5']
qid = []
response = []

row = []

#create docx document
doc = docx.Document()

#two list for noauth and auth scan
vulnerabilities_noauth = []
vulnerabilities_auth = []
#choose column in base of number
#row[0] == results (one row less)
#row[1] == ID
#row[3] == QID
#row[32] == CVSS V3 base
flagauth = False
flagNoauth = False

flagBacknoauth = False
flagBackauth = False
#initialaze cvss data
cvss_max = 1
cvss_min = 10
filename = ''

#row[22] javascript libraries in response
#after glossary QID
#row[1] == qid
#row[2] == title
#row[3] == category (ig, confirmed, potential)
#row[4] == severity level
#row[14] == cvssv3 base

#line[13] title of level 5,4,3 and line below the number
#row[2] level 5, row[3] level 4, row[4] level 3



def clearcategory():
    global cvss_min, cvss_max, qid, response
    response.clear()
    qid.clear()
    vulnerabilities_noauth.clear()
    vulnerabilities_auth.clear()
    cvss_max = 5
    cvss_min = 5

def uniquecategory(qid):
    qid = list(dict.fromkeys(qid))


#def category based on title of vulnerability
def categorization(row, auth):
    global cvss_min, cvss_max
    #check cvss and find more than number x
    if(row[0] == "VULNERABILITY" and row[32] != ''):
        if(float(row[32]) > 3.0):
            #take cvss_max and cvss_min

            if(cvss_min > float(row[32])):
                cvss_min = float(row[32])
            if(cvss_max < float(row[32])):
                cvss_max = float(row[32])

            qid.append(row[3])
            #take qid and response on column response
            response.append([ row[3], row[22] ])

    #check based on qid in glossary
    if(auth):
        if(row[0] == "QID" and row[4] in vulnSeverity and row[3] == "Confirmed Vulnerability"):
            vulnerabilities_auth.append([ row[1], row[2] ])

    else:
        if(row[0] == "QID" and row[4] in vulnSeverity and row[3] == "Confirmed Vulnerability"):
            vulnerabilities_noauth.append([ row[1], row[2] ])


#read csv and parsing and remove header from qualys
def readFile(filescsv):
    global source,filename,MAXint
    reader =''
    source = open(filescsv, "r", encoding="latin-1")
    reader = csv.reader(x.replace('\0', '') for x in source)

    #find if there is scan is auth or not
    #or check in name in findall
    for i in reader:
        try:
            csv.field_size_limit(MAXint)
            if(i[9] == "Authentication Record"):

                auth = next(reader)
                print(auth[9])
                if(auth[9] == "None"):
                    auth = False
                else:
                    auth = True
                break
        except OverflowError:
            MAXint = int(MAXint/10)
        except IndexError:
            pass

    #come back to beginning of file
    source.seek(0)

    #remove header from qualys
    #if the header is already removed comment this section
    #and take name from other source
    for i in range(17):
        if(i == 7):
            filename = row[1]
        #name of tags rename file with this
        #and take number of active host
        if(i == 13):
            secrisk = str(row[0]) #security risk
            nvuln = str(row[1]) #number of vulnerabilities
        #print(next(reader))
        row = next(reader)
    row = next(reader)

    return source,filename,row,reader,secrisk,nvuln,auth

#create a docx and write a analysis
def writeon_docx(docname,auth,nvuln,secrisk):
    global doc,flagauth,flagNoauth,cvss_min,cvss_max


    #if both is true already wrote summary and title
    if(not(flagauth or flagNoauth)):
        #create title
        title = doc.add_heading('Analisi ' + docname, level=1 )


        #write header and footer
        writewast.headerfooter(doc)
        #write table
        writewast.tab(doc)
        #write the readme
        writewast.readme(doc)


    if(not auth):
        writewast.noauth(doc, nvuln, secrisk, str(cvss_min), str(cvss_max), vulnerabilities_noauth, response)
        flagNoauth = True
    else:
        writewast.auth(doc, nvuln, secrisk, str(cvss_min), str(cvss_max), vulnerabilities_auth, response)
        flagauth = True

    #TODO write backend part

    #comment line below if only one file csv
    #if(flagauth and flagNoauth):

    writewast.backend(doc)

    docname = "analisi " + docname + ".docx"

    doc.save('./report_wast/' + docname)


def main():
    global row,filename,source,cvss_max,cvss_min
    #for every file csv in directory
    for filescsv in files:

        #read file csv and parsing
        source,filename,row,reader,secrisk,nvuln,auth = readFile(filescsv)

        #create a folder with name report in same directory
        if (os.path.isdir('report_wast')):
            pass
        else:
            os.mkdir('report_wast')


        #first line
        #reader is an iterator, for every rows make categorization
        try:
          for row in reader:
              categorization(row, auth)
        except IndexError:
          pass

        #close csv file
        source.close()

        #sorting category
        uniquecategory(qid)

        #create a file docx for analyze
        writeon_docx(filename, auth, nvuln, secrisk)


        #clear all category for next file
        clearcategory()

        print("done ->" + filename)



if __name__ == '__main__':
    main()
