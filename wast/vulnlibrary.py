

#for each qid associated a text for

# if qid then write specific text in doc

#return a text
def textoqid(qid, response):

    text = ''

    if(qid == "150003"):
        text = "Minaccia:\n" +\
                "SQL injection consente a un utente malintenzionato di modificare la sintassi di una query SQL per recuperare, corrompere o eliminare i dati." + \
                "Questo si ottiene manipolando query  in un modo che influenza la logica della query." + \
                "Le cause tipiche di questa vulnerabilità sono la mancanza di convalida di input e la costruzione insicura della query SQL." + \
                "Impatto:\n" + \
                "La portata di un exploit SQL injection varia notevolmente. Se qualsiasi istruzione SQL può essere iniettata nella query," + \
                "allora l'attaccante ha l'accesso equivalente di un amministratore del database. Questo accesso potrebbe portare a furto di dati," + \
                "danneggiamento dannoso dei dati  o la cancellazione dei dati stessi." + \
                "Possibile soluzione:\n" + \
                "Tutti gli input ricevuti dal client Web devono essere convalidati per il contenuto corretto." + \
                "Le procedure memorizzate sono query precompilate che risiedono nel database. Come le istruzioni preparate," + \
                "esse impongono anche la separazione dei dati delle query e della logica. Le SQL statements, altrimenti la sicurezza di quest’ultima sarebbe negata. " + \
                "Gli exploit di SQL injection possono essere mitigati mediante l'utilizzo di Access Control List o l'accesso basato sui ruoli all'interno del database."
    elif(qid == "150004"):
        text = "Minaccia:\n" + \
                "Contenuto confidenziale o directory o ulteriori informazioni possono essere svelate navigando il path del web server.\n" + \
                "Possibile soluzione:\n" + \
                "Verificare se l'accesso è permesso a queste directory e eventualmente bloccare l’accesso."
    elif(qid == "150023"):
        text = "Minaccia:\n" + \
                "Il web server presenta una lista di file e i contenuti potrebbero essere confidenziali\n" + \
                "Possibile soluzione:\n" + \
                "verificare il file excel ed eventualmente proteggere i link interessati:"
    elif(qid == "150022"):
        text = "Minaccia:\n" + \
                "Messaggi di errore prolissi spesso espongono dettagli tecnici che sono utili per gli attaccanti.\n" + \
                "Possibile soluzione:\n" + \
                "Implementare la gestione degli errori e delle eccezioni per garantire che l'applicazione " + \
                "Web visualizzi solo messaggi di errore generici. Evitare di restituire le stack traces, debugging delle informazioni, o altri dettagli tecnici per il cliente."
    elif(qid  == "150053"):
        text = "Nel form di login ci sono dei link che vengono serviti tramite http, sono esposti a sniffing, servire il sito tramite https."
    elif(qid == "150124"):
        text = "Può essere inserito un frame invisibile cliccabile da un utente.\n" + \
                "Una soluzione può essere evitare il clickjacking con le intestazioni x-frame-options o content-security-policy"
    elif(qid == "150112"):
        text = "Un modulo HTML che raccoglie informazioni sensibili non impedisce al browser " + \
                "di chiedere all'utente di salvare i valori popolati per riutilizzarli in seguito, se " + \
                "la postazione di lavoro è condivisa può essere usato l’autocompletamento per inserire " + \
                "le credenziali anche se non si è l’utente autorizzato." + \
                "Una soluzione potrebbe essere quella di aggiungere l’attributo “autocomplete=off”, questo attributo evita al browser di salvare le credenziali."
    elif(qid == "150145"):
        text = "“Mixed content”  viene rilevato durante il caricamento della pagina, " + \
                "significa che la pagina viene servita tramite https ma i contenuti addizionali vengono " + \
                "serviti su un canale non sicuro. Un comportamento associato al browser mozilla firefox, " + \
                "questa vulnerabilita’ viene riportata se qualche contenuto e’ veicolato attraverso questo " + \
                "canale non sicuro:  script, link, iframe, XMLHttpRequest requests, object, applet.\n" + \
                "Il canale non sicuro è vulnerabile ad attacchi sniffing, possono ascoltare il canale e avere accesso a dati.\n" + \
                "Servire le sotto risorse tramite https."
    elif(qid == "150161"):
        text = "Richieste contenenti i Cookie di sessione potrebbero essere reindirizzate  in chiaro tramite http.\n" + \
                "Una soluzione può essere applicare l’attributo “secure” al cookie in modo che venga inviato solo tramite https."
    elif(qid == "150263"):
        text = "*****IMPORTANTE******nessuna redirezione obbligata da http a https"
    elif(qid == "150210"):
        text = "’header di risposta del server rilascia informazioni sulle tecnologie usate attraverso le intestazioni: 'Server', 'X-Powered-By', 'X-AspNetVersion', 'X-AspNetMvcVersion’\n" + \
                "Una soluzione può essere disabilitare questi header nella risposta."
    elif(qid == "150151"):
        text = "Minaccia:\n" + \
                "Vulnerabile al packet sniffing per il furto delle credenziali \n" + \
                "Soluzione:\n " + \
                "La chiamata di autenticazione al server dovrebbe essere cifrata dal protocollo TLS"
    elif(qid == "150162"):
        text = "Le vulnerabilità più rilevanti riguardano l’utilizzo di versioni vulnerabili di librerie JavaScript.\n" + \
                "Di seguito elenchiamo le librerie vulnerabili individuate:"
    elif(qid == "150120"):
        text = "Richieste contenenti i Cookie di sessione potrebbero essere reindirizzate in chiaro tramite http.\n" + \
                "Una soluzione può essere applicare l’attributo “secure” al cookie di sessione in modo che venga inviato solo tramite https."
    elif(qid == "150121"):
        text = "Il cookie di sessione utilizzato per identificare gli utenti autenticati, non contiene l’attributo “HTTPOnly” " + \
                "Cookie senza questo attributo possono essere rubati ed usati per impersonare un utente." + \
                "Una possibile soluzione sarebbe di utilizzare l’attributo sul cookie di sessione."
    elif(qid == "150122"):
        text = "Richieste contenenti i Cookie potrebbero essere reindirizzate  in chiaro tramite http.\n" + \
                "Una soluzione può essere applicare l’attributo “secure” al cookie in modo che venga inviato solo tramite https."
    elif(qid == "150123"):
        text = "Il cookie non contiene l’attributo “HTTPOnly” che permette di essere acceduto tramite javascript, attacchi di tipo XSS possono esfiltrare il cookie dell’utente.\n" + \
                "Si consiglia di aggiungere l’attributo  “HTTPOnly”  al cookie se il rischio associato ad una compromissione di un account è troppo alto."
    elif(qid == "150150"):
        text = "Il form html contiene il campo “type=password”, e viene esposto in http che lascia esposto ad attacchi di tipo “phising”.\n" + \
                "Far passare le credenziali attraverso una cifratura ssl/tls utilizzata in https."

    elif(qid == "150001"):
        text = "Threat:\n" + \
                "Le vulnerabilità XSS si verificano quando l'applicazione Web riprende i dati forniti dall'utente " + \
                "in una risposta HTML inviata al browser Web. Ad esempio, un'applicazione Web potrebbe includere " + \
                "il nome dell'utente come parte di un messaggio di benvenuto o visualizzare l'indirizzo di casa quando " + \
                "si conferma una destinazione di spedizione. Se i dati forniti dall'utente contengono caratteri che vengono " + \
                "interpretati come parte di un elemento HTML invece che come testo letterale, un aggressore può modificare " + \
                "l'HTML ricevuto dal browser Web della vittima. Il payload XSS viene riprodotto nel documento HTML restituito dalla richiesta." + \
                "Un payload XSS può essere costituito da HTML, JavaScript o altro contenuto che verrà reso dal browser." + \
                "Per sfruttare questa vulnerabilità, un utente malintenzionato dovrebbe indurre la vittima a visitare l'URL con il payload XSS. \n" + \
                "Impact:\n" + \
                "Gli exploit XSS rappresentano una minaccia significativa per un'applicazione Web, per i suoi utenti e per i dati degli utenti. " + \
                "Gli exploit XSS prendono di mira gli utenti di un'applicazione Web piuttosto che l'applicazione Web stessa. " + \
                "Un exploit può portare al furto delle credenziali dell'utente e delle informazioni personali o finanziarie. " + \
                "Gli exploit e gli scenari di attacco complessi sono possibili tramite XSS perché consentono all'aggressore di eseguire codice dinamico." + \
                "Solution:\n" + \
                "Filtrare tutti i dati raccolti dal client, compresi i contenuti forniti dall'utente e quelli del browser, " + \
                "come le intestazioni Referrer e User-Agent. Tutti i dati raccolti dal client e visualizzati in una pagina Web " + \
                "devono essere codificati in HTML per garantire che il contenuto sia reso come testo anziché come elemento HTML o JavaScript."
    elif(qid == "150071"):
        text = "Threat:\n" + \
                "È stato rilevato un modulo che non sembrava essere completamente protetto contro il cross-site request forgery (CSRF)."+ \
                "Il modulo è stato testato per verificare la suscettibilità a un attacco CSRF ed è risultato vulnerabile." + \
                "Impact:\n" + \
                "Le vulnerabilità CSRF possono essere utilizzate da un aggressore per costringere un utente a inviare richieste " + \
                "all'applicazione web senza che l'utente ne sia a conoscenza o abbia dato la sua approvazione." + \
                "Solution:\n" + \
                "La contromisura CSRF standard consiste nell'includere un campo modulo nascosto con un valore casuale specifico " + \
                "della sessione corrente dell'utente, che viene poi convalidato dall'applicazione sul lato server. " + \
                "Tutti i moduli dell'applicazione devono essere protetti dagli attacchi CSRF. Al seguente link informazioni " + \
                "dettagliate sulle contromisure per CSRF: https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html."
    elif(qid == "150013"):
        text = "Threat:\n" + \
                "La vulnerabilità XSS occorre quando la webapp risponde ad un input dato dall’utente tramite html, " + \
                "come un messaggio di benvenuto con il nome dell’utente." + \
                "Il payload XSS viene ripetuto dal documento html alla risposta del server." + \
                "Un payload puo consistere in html o javascript che viene reindirizzata dal browser, un utente malevolo può inviare il link con il payload XSS." + \
                "Una possibile soluzione e’ filtrare tutti i dati che il client e il browser invia come Referrer e User-Agent headers." + \
                "Ogni dato deve essere codificato con html e’ renderizzato come testo invece di elemento HTML."
    elif(qid == "150051"):
        text = "Threat:" + \
                "La webapp crea una redirezione da un form o una querystring, la destinazione di redirezione può essere cambiata." + \
                "La redirezione e’ usata per inviare link con il sito con una redirezione su un sito non correlato alla webapp." + \
                "Verificare la redirezione se e’ accettabile su determinati host."
    elif(qid == "151025"):
        for q in response:
            if q[0] == qid:
                text = q[1]
    elif(qid == "151015"):
        for q in response:
            if q[0] == qid:
                text = q[1]
    elif(qid == "151002"):
        for q in response:
            if q[0] == qid:
                text = q[1]
    elif(qid == "151024"):
        for q in response:
            if q[0] == qid:
                text = q[1]
    elif(qid == "151000"):
        for q in response:
            if q[0] == qid:
                text = q[1]
    else:
        text = ""


    return text
